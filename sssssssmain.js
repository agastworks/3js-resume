import './style.css';

import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer({
  canvas: document.querySelector('#bg'),
});

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
camera.position.setZ(50);
camera.position.setY(0);
camera.position.setX(0);

renderer.render(scene, camera);

// ... (torus, lighting, stars, background, logoBox code)

const controls = new OrbitControls(camera, renderer.domElement);

const textData = [
  "ANDREW GASTÓN | Software Engineer",
  "Salt Lake City, Utah · 801-979-2985",
  //... more text lines
];

const loader = new THREE.FontLoader();
loader.load('https://threejs.org/examples/fonts/helvetiker_regular.typeface.json', (font) => {
  const textGeometry = textData.map((text, index) => {
    const geometry = new THREE.TextGeometry(text, {
      font: font,
      size: 1,
      height: 0.1,
    });
    return geometry;
  });

  const textMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff });
  const textMeshes = textGeometry.map((geometry) => {
    const mesh = new THREE.Mesh(geometry, textMaterial);
    return mesh;
  });

  let yPos = 0;
  textMeshes.forEach((mesh) => {
    mesh.position.set(-20, yPos, -50);
    scene.add(mesh);
    yPos -= 2;
  });

  function animate() {
    requestAnimationFrame(animate);

    torus.rotation.x += 0.021;
    torus.rotation.y += 0.005;
    torus.rotation.z -= 0.002;
    logoBox.rotation.x -= 0.021;
    logoBox.rotation.y -= 0.005;
    logoBox.rotation.z += 0.002;

    textMeshes.forEach((mesh, index) => {
      mesh.position.z += 0.05;
      if (mesh.position.z > 50) {
        mesh.position.z = -50;
      }
    });

    controls.update();
    renderer.render(scene, camera);
  }

  animate();
});















// const monolithWallMaterial = new THREE.MeshStandardMaterial({ color : 0xffffff});
// const monolithWallGeometry = new THREE.PlaneGeometry(30, 30);
// const loadFont = new THREE.FontLoader()
// loadFont.load('http://three.js.org/examples/fonts/helvetiker_regular.typeface.json', (font) => {
//   const textGeometry = new THREE.TextGeometry(sc)
// })
