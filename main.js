import './style.css'

import * as THREE from 'three';
import { AmbientLight } from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js'

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGL1Renderer({
  canvas: document.querySelector('#bg'),
});

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
camera.position.setZ(36);
camera.position.setY(15);
camera.position.setX(-15);

renderer.render(scene, camera);

const geometry = new THREE.TorusGeometry( 10, 2, 11, 97);
const material = new THREE.MeshStandardMaterial({ color: 0x0080FF});
const torus = new THREE.Mesh( geometry, material);
torus.position.set(17,6,-4);

scene.add(torus)

const ambientLight = new THREE.AmbientLight
const pointLight = new THREE.PointLight(0xffffff)
pointLight.position.set(5, 8, 2)
scene.add(pointLight, ambientLight)

const lightHelper = new THREE.PointLightHelper(pointLight);
const gridHelper = new THREE.GridHelper(200, 50);
scene.add(lightHelper, gridHelper)

const controls = new OrbitControls(camera, renderer.domElement);

/*THIS CREATES THE STARS */
function addStars(){
  const geometry = new THREE.SphereGeometry(0.25, 24, 24);
  const material = new THREE.MeshStandardMaterial({ color: 0xffffff })
  const star = new THREE.Mesh(geometry, material);

  const[x, y, z] = Array(3).fill().map(() => THREE.MathUtils.randFloatSpread (100))
  star.position.set(x, y, z)
  scene.add(star)
}
/**CREATE AN ARRAY OF 200 INDECICES; EACH INDEX IN THE ARRAY HOLDS A RANDOMLY GERERATED STAR OBJECT */
Array(200).fill().forEach(addStars)

const cyberSpaceTexture = new THREE.TextureLoader().load('new_space.jpg');
scene.background = cyberSpaceTexture;



/**CREATE LOGO BOX  */
const textureLogo = new THREE.TextureLoader().load('logo.svg');
const logoBox = new THREE.Mesh(
  new THREE.BoxGeometry(4, 4, 4),
  new THREE.MeshBasicMaterial({map: textureLogo })
);
logoBox.position.set(17,6,-4);
scene.add(logoBox)

/** CREATE RESUME INFO BOXES */

// /** ADD MONOLITH WALL */
const monolithWallGeometry = new THREE.BoxGeometry(25, 50, 10, 50, 10, 50);
const monolithWallMaterial = new THREE.MeshBasicMaterial({ color : 0xffffff , wireframe : true});
const monolithWall = new THREE.Mesh(monolithWallGeometry, monolithWallMaterial);
monolithWall.position.set(-17, 4, 0);
scene.add(monolithWall)

/**THIS IS RECURSIVE FUNCTION TO ANIMATE THE TORUS --- ADDED FUNCTIONALITY */
function animate(){
  requestAnimationFrame(animate);

  torus.rotation.x += 0.021;
  torus.rotation.y += 0.005;
  torus.rotation.z -= 0.002;
  /** ADDED FUNCTIONALITY TO ROTATE LOGOBOX COUNTER TO TORUS SPIN */
  logoBox.rotation.x -= 0.021;
  logoBox.rotation.y -= 0.005;
  logoBox.rotation.z += 0.002;

  controls.update()
  renderer.render(scene, camera);
}

/**ANIMATE MONOWALL INDEPENDANTLY OF THE REST OF THE SCENE TO
   KEEP REFERENCED AND SCROLL UP/DOWN vs. IN/OUT */
function animateMonoWall(){
  requestAnimationFrame(animateMonoWall);

  monolithWall.lookAt(camera.position);

}

animate()
animateMonoWall()
